﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

using taskmanager_with_asp.net.Models;
using taskmanager_with_asp.net.DbStorage;

namespace taskmanager_with_asp.net.Controllers
{
    public class HomeController : Controller
    {
        private readonly Storage _storage;
        private DateTime _dateTime;
        public HomeController(Storage storage)
        {
            _storage = storage;
            _dateTime = DateTime.Now;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _storage.Tasks.ToListAsync());
        }
        [HttpPost]
        public async Task<IActionResult> Create (string NameTask, string GoalTasks)
        {
            var Task = new Tasks()
            {
                NameTask = NameTask,
                GoalTasks = GoalTasks,
                status = "Ожидание"
            };
            _storage.Tasks.Add(Task);
            _storage.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> Play (int Id)
        {
            var task = _storage.Tasks.First(p => p.Id == Id);
            task.status = "В работе";
            task.DatetimeStart = _dateTime;
            _storage.SaveChanges();
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Delete(int Id)
        {
            var task = _storage.Tasks.First(p => p.Id == Id);
            _storage.Remove(task);
            _storage.SaveChanges();
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Pause(int Id)
        {
            var task = _storage.Tasks.First(p => p.Id == Id);
            task.status = "Пауза";
            task.DatetimePause += _dateTime - task.DatetimeStart;
            _storage.SaveChanges();
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Done(int Id)
        {
            var task = _storage.Tasks.First(p => p.Id == Id);
            task.status = "Выполнено";
            task.DateTimeEnd = _dateTime;
            _storage.SaveChanges();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}