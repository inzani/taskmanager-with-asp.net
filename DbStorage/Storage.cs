﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using taskmanager_with_asp.net.Models;

using Microsoft.EntityFrameworkCore;

namespace taskmanager_with_asp.net.DbStorage
{
    public class Storage : DbContext
    {
        public DbSet<Tasks> Tasks { get; set; }

        public Storage(DbContextOptions<Storage> options)
        : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
