﻿namespace taskmanager_with_asp.net.Models
{
    public class Tasks
    {
        public int Id { get; set; }
        public string NameTask { get; set; }
        public string GoalTasks { get; set; }
        public string status { get; set; }
        public DateTime DatetimeStart { get; set; }
        public DateTime DateTimeEnd { get; set; }
        public TimeSpan DatetimePause { get; set; }
    }
}
